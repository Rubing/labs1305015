package lab2.coffeemachine;

import java.util.Scanner;

public class CoffeeMachine {
    
    Scanner reader = new Scanner(System.in);
    
    private boolean powerOn;
    private int waterTank;
    private int coffeeBeanTank;
    private int runTimes;
    private final int MAX_RUN_TIMES = 4;
    private final int RINSE_AMOUNT = 25;
    
    public CoffeeMachine() {
        powerOn = false;
        waterTank = 0;
        coffeeBeanTank = 0;
        runTimes = 0;
    }
    
    public void turnOn() {
        powerOn = true;
        System.out.println("The coffee machine is on.");
        powerOn = rinse();
        if (!powerOn) {
            System.out.println("Power is off.");
        }
    }
    
    private boolean rinse(){
        
        if (waterTank >= RINSE_AMOUNT) {
            waterTank -= RINSE_AMOUNT;
            System.out.println("It is rising, please wait a moment...");
            runTimes = 0;
            return true;
        } else {
            System.err.println("Please fill in the water tank for rinse!");
            return false;
        }
    }
    
//    public void clean(int amtClean){
//        
//        if (waterTank >= 300) {
//            waterTank -= amtClean;
//            System.out.println("Coffee machine is under cleaning process...");
//        } else {
//            System.err.println("Please fill in the water tank for cleaning!");
//        }
//    }
    
    public void turnOff() {
        powerOn = false;
        rinse();
        System.out.println("The coffee machine is off.");
    }
    
    public void makeCoffee() {
        
        if (!powerOn) {
            System.err.println("The coffee machine is turned off");
            return;
        }
        
        if (runTimes == MAX_RUN_TIMES) {
            System.out.println("Coffee machine needs a cleaning!");
            return;
        }
        
        System.out.println("Which coffee do you want, expresso or regular?");
        String coffeeName = reader.nextLine();
        
        if (coffeeName.equals("expresso")) {
            if (coffeeBeanTank >= 8 && waterTank >= 30) {
                coffeeBeanTank -= 8;
                waterTank = waterTank - 30;
                System.out.println("Coffee machine is making expresso...");
            } else if (coffeeBeanTank < 8) {
                System.err.println("Please add coffee beans.");
                return;
            } else {
                System.err.println("Please fill the water tank.");
                return;
            }
        } else if (coffeeName.equals("regular")){
            if (coffeeBeanTank >= 10 && waterTank >= 150) {
                coffeeBeanTank -= 10;
                waterTank = waterTank - 150;
                System.out.println("Coffee machine is making regular coffee...");
            } else if (coffeeBeanTank < 10) {
                System.err.println("Please add coffee beans.");
                return;
            } else {
                System.err.println("Please fill the water tank.");
                return;
            }
        } else {
            System.out.println(coffeeName + " is not avaliable");
            return;
        }
        
        runTimes++;
    }
    
    
//        while (runTimes <= 4) {
//
//            if (coffeeName.equals("expresso") && coffeeBeanTank >= 8 && waterTank >= 30) {
//                coffeeBeanTank -= 8;
//                waterTank = waterTank - 30;
//                System.out.println("Coffee machine is making expresso...");
//            } else if (coffeeName.equals("regular") && coffeeBeanTank >= 10 && waterTank >= 150) {
//                coffeeBeanTank = coffeeBeanTank - 10;
//                waterTank = waterTank - 150;
//                System.out.println("Coffee machine is making regular coffee...");
//            } else {
//            System.err.println( coffeeName + " is not available. ");
//            }
//                break;
//        }
    
//    public void askInstruction() {
//        System.out.println("Do you want to");
//    }
    
    public void fillWaterTank(){
        waterTank = 710;
        System.out.println("Water tank is filled to max volume.");
    }
    
    public void fillCoffeeBean() {
        coffeeBeanTank = 72;
        System.out.println("Coffee bean is filled to max volume.");
    }
    
//    public void checkTimes() {
//        if (runTimes == MAX_RUN_TIMES) {
//            System.out.println("Coffee machine needs a cleaning!");
//        } else {
//            runTimes ++;
//        }
//    }
}
