
package lab3.cdplayer;

public class Track {
    
    private String tracksName;
    private double tracksLength;
    
    public Track(String tracksName, double tracksLength){
        this.tracksName=tracksName;
        this.tracksLength=tracksLength;
    }
    
    public String getName() {
        return tracksName;
    }
    
    public double getLength() {
        return tracksLength;
    }
    
    public String toString(){
        return tracksName + " (" + tracksLength + ")";
    }
    
}
