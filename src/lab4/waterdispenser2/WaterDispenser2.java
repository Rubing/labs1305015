package lab4.waterdispenser2;

import java.util.Scanner;


public class WaterDispenser2 {
    
    private boolean powerOn;
    private boolean valveOpen;
    private boolean lidOn;
    private int waterLevel;
    
    public WaterDispenser2() {
        powerOn=false;
        valveOpen=false;
        lidOn=false;
        waterLevel=0;
    }
    
    
    
    public void turnOn(){
        powerOn=true;
        System.out.println("The water dispenser is on. ");
    }
    public void turnOff(){
        powerOn=false;
        System.out.println("The water dispenser is off. ");
    }
    
    public void openValve() {
        valveOpen = true;
        System.out.println("The valve is now open.");
    }
    
    public void closeValve() {
        valveOpen = false;
        System.out.println("The valve is now closed.");
    }
    
    public void closeLid(){
        lidOn=true;
        System.out.println("Water tank is closed.");
    }
    
    public void openLid(){
        lidOn=false;
        System.out.println("Water tank is open.");
    }
    
    public void fillWaterTank(){
        if (!lidOn) {
            waterLevel=100;
            System.out.println("Water level is now " + waterLevel);
        } else {
            System.err.println("The lid is on. Filling the water tank can not be executed.");
        }
    }
    
    public int getWaterLevel() {
        return waterLevel;
    }
    
    public void dispenseWater(){
        if (!powerOn) {
            System.err.println("The water dispenser is off!");
            return;
        }
        if (!valveOpen) {
            System.err.println("The valve is closed!");
            return;
        }
        System.out.print("How much water do you want? ");
        Scanner reader = new Scanner(System.in);
        int amount = Integer.parseInt(reader.nextLine());
        if (amount <= 0) {
            System.err.println("You are an idiot! Amount needs to be positive.");
        } else if (waterLevel == 0) {
            System.out.println("Tank is empty. Please fill the water tank.");
        } else if (waterLevel >= amount) {
            int dispensingAmount = amount;
            while (dispensingAmount > 0) {
                System.out.println("*");
                int amount2 = Math.min(10, dispensingAmount);
                waterLevel -= amount2;
                dispensingAmount -= amount2;
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ex) {
                    System.err.println("Something went wrong!");
                }
            }
        } else {
            System.err.println("There is only " + waterLevel + " left in the tank!");
        }
    }
    
}
