
package lab3.cdplayer;

import java.util.ArrayList;

public class Main {
    public static void main(String args[]) {
        
        Track track1 = new Track("Viva la Vida", 4.0);
        Track track2 = new Track("Violet Hill", 3.7);
        Track track3 = new Track("Princess of China", 3.89);
        Track track4 = new Track("Paradise", 4.60);
        
        ArrayList<Track> list1 = new ArrayList<Track>();
        ArrayList<Track> list2 = new ArrayList<Track>();
        
        list1.add(track1);
        list1.add(track2);
        list2.add(track3);
        list2.add(track4);
        
        CD cd1 = new CD("Viva la Vida", "Coldplay", list1);
        CD cd2 = new CD("Mylo Xyloto", "Coldplay", list2);
        
        CDPlayer BeatsCDPlayer = new CDPlayer();
        
        BeatsCDPlayer.powerOn();
        BeatsCDPlayer.insert(cd1);
        BeatsCDPlayer.playPause();
        BeatsCDPlayer.next();
        BeatsCDPlayer.next();
        BeatsCDPlayer.previous();
        BeatsCDPlayer.previous();
        BeatsCDPlayer.previous();
        BeatsCDPlayer.eject();
        BeatsCDPlayer.insert(cd2);
        BeatsCDPlayer.playPause();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeminus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.volumeplus();
        BeatsCDPlayer.mute();
        BeatsCDPlayer.unmute();
        BeatsCDPlayer.stop();
        BeatsCDPlayer.powerOff();
    }
    
}