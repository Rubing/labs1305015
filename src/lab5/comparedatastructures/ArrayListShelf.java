
package lab5.comparedatastructures;

import java.util.ArrayList;

public class ArrayListShelf extends Shelf {

    private ArrayList<CD> list;
    
    public ArrayListShelf() {
        this.list = new ArrayList<CD>();
    }
    
    @Override
    public void insert(CD cd) {
        // find the position for the CD using binary search
        int index = this.binarySearch(cd);
        this.list.add(index, cd);
    }

    @Override
    public CD get(int position) {
        return this.list.get(position);
    }
    
    private int binarySearch(CD cd) {
        int low = 0;
        int high = this.list.size() - 1;
        while(low <= high) {
            int mid = (low + high) / 2;
            CD midCD = this.list.get(mid);
            if(midCD.compareTo(cd) < 0) low = mid + 1;
            else if(midCD.compareTo(cd) > 0) high = mid - 1;
            else return mid;
        }
        return low;
    }
    
}
