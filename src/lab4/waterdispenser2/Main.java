
package lab4.waterdispenser2;

import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        
        
        WaterDispenser2 thisWaterDispenser = new WaterDispenser2 ();
        Scanner reader = new Scanner(System.in);
        
        thisWaterDispenser.turnOn();
        
        thisWaterDispenser.openLid();
        
        thisWaterDispenser.fillWaterTank();
        
        thisWaterDispenser.closeLid();
        
        thisWaterDispenser.openValve();
        
        while (true) {
            if(thisWaterDispenser.getWaterLevel() > 0) {
                thisWaterDispenser.dispenseWater();
                System.out.println("Do you still want water? (y/n)");
                if(reader.nextLine().contains("n")) {
                    break;
                }
            } else {
                System.out.println("Do you want to fill the water tank? (y/n)");
                if(reader.nextLine().contains("n")) {
                    break;
                } else{
                    thisWaterDispenser.openLid();
                    thisWaterDispenser.fillWaterTank();
                    thisWaterDispenser.closeLid();
                }
            }
            
        }
        
        thisWaterDispenser.closeValve();
        
        thisWaterDispenser.turnOff();
        
    }
    
}
