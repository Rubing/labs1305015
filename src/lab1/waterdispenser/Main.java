
package lab1.waterdispenser;

public class Main {
    public static void main(String args[]) {
        
        WaterDispenser thisWaterDispenser = new WaterDispenser ();
        
        thisWaterDispenser.turnOn();
        
        thisWaterDispenser.openLid();        
               
        thisWaterDispenser.fillWaterTank();
        
        thisWaterDispenser.closeLid();
        
        thisWaterDispenser.dispenseWater(10);
        
        thisWaterDispenser.turnOff();
        
    }
    
}
