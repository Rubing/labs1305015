
package lab5.comparedatastructures;


public class CD implements Comparable<CD> {
    
    private final String CDName;
    private final String artist;
    
    public CD (String CDName, String artist) {
        this.CDName=CDName;
        this.artist=artist;
    }
            
    public String getName(){
        return CDName;
    }
    
    public String getArtist(){
        return artist;
    }
    
    @Override
    public int compareTo(CD another) {
        return this.CDName.compareTo(another.CDName);
    }
    
    @Override
    public String toString(){
        return artist + " - " + CDName ;
    }
    
}