
package lab2.coffeemachine;

public class Main {
    public static void main(String args[]) {
        
        CoffeeMachine mochaMaster = new CoffeeMachine();
        
//        mochaMaster.turnOn();
//        
//        mochaMaster.rinse(25);
//        
//        mochaMaster.fillWaterTank();
//        
//        mochaMaster.fillCoffeeBean();
//        
//        mochaMaster.makeCoffee();  
//        
//        mochaMaster.checkTimes();
//        
//        mochaMaster.clean(300);
//        
//        mochaMaster.rinse(25);
//        
//        mochaMaster.turnOff();
        
        mochaMaster.turnOn();
        mochaMaster.fillWaterTank();
        mochaMaster.turnOn();
        mochaMaster.makeCoffee();
        mochaMaster.fillCoffeeBean();
        mochaMaster.makeCoffee();
        mochaMaster.makeCoffee();
        mochaMaster.makeCoffee();
        mochaMaster.makeCoffee();
        mochaMaster.makeCoffee();
        mochaMaster.turnOff();
        mochaMaster.fillWaterTank();
        mochaMaster.turnOn();
        
        
    }
    
}
