
package lab5.comparedatastructures;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class HashSetShelf extends Shelf {

    private HashSet<CD> list;
    
    public HashSetShelf() {
        this.list = new HashSet<CD>();
    }
    
    @Override
    public void insert(CD cd) {
        this.list.add(cd);
    }

    @Override
    public CD get(int position) {
        TreeSet<CD> set = new TreeSet<CD>();
        set.addAll(this.list);
        int index = 0;
        CD cd = set.first();
        while (index < position) {
            cd = set.higher(cd);
            index ++;
        }
        return cd;
    }

}
