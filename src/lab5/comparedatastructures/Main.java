
package lab5.comparedatastructures;

public class Main {
    
    public static void main(String[] args) {
        String[][] cdData = {
            // {"CD NAME", "ARTIST NAME"},
            {"WE THE GENERATION", "RUDIMENTAL"},
            {"X", "ED SHEERAN"},
            {"I CRY WHEN I LAUGH", "JESS GLYNNE"},
            {"RATTLE THAT LOCK", "DAVID GILMOUR"},
            {"IN DREAM", "EDITORS"},
            {"BEAUTY BEHIND THE MADNESS", "WEEKND"},
            {"IN THE LONELY HOUR", "SAM SMITH"},
            {"CARACAL", "DISCLOSURE"},
            {"STORIES", "AVICII"},
            {"CHAOS AND THE CALM", "JAMES BAY"},
            {"UNBREAKABLE", "JANET JACKSON"},
            {"CRADLE TO THE GRAVE", "SQUEEZE"},
            {"75 AT 75 - 75 CAREER SPANNING HITS", "CLIFF RICHARD"},
            {"KEEP THE VILLAGE ALIVE", "STEREOPHONICS"},
            {"MUSIC COMPLETE", "NEW ORDER"},
            {"BRAVE", "SHIRES"},
            {"1989", "TAYLOR SWIFT"},
            {"HONEYMOON", "LANA DEL REY"},
            {"SILENCE IN THE SNOW", "TRIVIUM"},
            {"PSYCHIC WARFARE", "CLUTCH"},
            {"HOW BIG HOW BLUE HOW BEAUTIFUL", "FLORENCE & THE MACHINE"},
            {"CASS COUNTY", "DON HENLEY"},
            {"THAT'S THE SPIRIT", "BRING ME THE HORIZON"},
            {"COMMUNION", "YEARS & YEARS"},
            {"EVERY OPEN EYE", "CHVRCHES"},
            {"FETTY WAP", "FETTY WAP"},
            {"NATHANIEL RATELIFF & THE NIGHT SWEATS", "NATHANIEL RATELIFF & THE NIGHT"},
            {"WILDER MIND", "MUMFORD & SONS"},
            {"WHAT A TIME TO BE ALIVE", "DRAKE & FUTURE"},
            {"CELEBRATION", "FOSTER & ALLEN"},
            {"THE BOOK OF SOULS", "IRON MAIDEN"},
            {"ZIPPER DOWN", "EAGLES OF DEATH METAL"},
            {"HOZIER", "HOZIER"},
            {"WRITTEN IN SCARS", "JACK SAVORETTI"},
            {"WHAT WENT DOWN", "FOALS"},
            {"ANTHEMS FOR DOOMED YOUTH", "LIBERTINES"},
            {"WANTED ON VOYAGE", "GEORGE EZRA"},
            {"TITLE", "MEGHAN TRAINOR"},
            {"THE LIGHT IN YOU", "MERCURY REV"},
            {"COMPTON", "DR DRE"},
            {"THE ULTIMATE COLLECTION", "PAUL SIMON"},
            {"BACK TO BLACK", "AMY WINEHOUSE"},
            {"RADIO CITY MUSIC HALL", "JOE BONAMASSA"},
            {"DRONES", "MUSE"},
            {"AMERICAN BEAUTY/AMERICAN PSYCHO", "FALL OUT BOY"},
            {"RIGHT HERE", "SHANE FILAN"},
            {"TUG OF WAR", "PAUL MCCARTNEY"},
            {"CROSSEYED HEART", "KEITH RICHARDS"},
            {"BADLANDS", "HALSEY"},
            {"GOLGOTHA", "WASP"},
            {"HOLLOW MEADOWS", "RICHARD HAWLEY"},
            {"FELINE", "ELLA EYRE"},
            {"GREATEST HITS", "BON JOVI"},
            {"+", "ED SHEERAN"},
            {"MOTHERS", "SWIM DEEP"},
            {"PIPES OF PEACE", "PAUL MCCARTNEY"},
            {"STAND BY ME - THE VERY BEST OF", "DRIFTERS"},
            {"IF YOU'RE READING THIS IT'S TOO LATE", "DRAKE"},
            {"FIRE WITHIN", "BIRDY"},
            {"THE VERY BEST OF", "FLEETWOOD MAC"},
            {"ROCKET MAN - THE DEFINITIVE HITS", "ELTON JOHN"},
            {"THE BALCONY", "CATFISH & THE BOTTLEMEN"},
            {"BEST OF - DECADE IN THE SUN", "STEREOPHONICS"},
            {"AM", "ARCTIC MONKEYS"},
            {"GREATEST HITS", "QUEEN"},
            {"ROYAL BLOOD", "ROYAL BLOOD"},
            {"THE DEFINITIVE COLLECTION", "STEVIE WONDER"},
            {"DODGE AND BURN", "DEAD WEATHER"},
            {"THE DEFINITIVE COLLECTION", "LIONEL RICHIE & THE COMMODORES"},
            {"B'LIEVE I'M GOIN DOWN", "KURT VILE"},
            {"PHARMACY", "GALANTIS"},
            {"VERDI/AIDA", "OR ACCADEMIA DI SANTA/PAPPANO"},
            {"THE ULTIMATE COLLECTION", "WHITNEY HOUSTON"},
            {"LEGEND", "BOB MARLEY & THE WAILERS"},
            {"HOT STREAK", "WINERY DOGS"},
            {"NUMBER ONES", "MICHAEL JACKSON"},
            {"CONDITION HUMAN", "QUEENSRYCHE"},
            {"1989", "RYAN ADAMS"},
            {"SOMEWHERE BACK IN TIME - THE BEST OF", "IRON MAIDEN"},
            {"21", "ADELE"},
            {"TIME FLIES - 1994-2009", "OASIS"},
            {"A PERFECT CONTRADICTION", "PALOMA FAITH"},
            {"HAVE YOU IN MY WILDERNESS", "JULIA HOLTER"},
            {"BURNING BRIDGES", "BON JOVI"},
            {"GREATEST HITS", "FOO FIGHTERS"},
            {"1000 FORMS OF FEAR", "SIA"},
            {"FOUR", "ONE DIRECTION"},
            {"GOLD - GREATEST HITS", "ABBA"},
            {"THE DARK SIDE OF THE MOON", "PINK FLOYD"},
            {"CURTAIN CALL - THE HITS", "EMINEM"},
            {"IRE", "PARKWAY DRIVE"},
            {"WHAT'S THE STORY MORNING GLORY", "OASIS"},
            {"ISLANDS", "BEAR'S DEN"},
            {"SMOKE & MIRRORS", "IMAGINE DRAGONS"},
            {"CHASING YESTERDAY", "NOEL GALLAGHER'S HIGH FLYING"},
            {"THE STONE ROSES", "STONE ROSES"},
            {"THREAT TO SURVIVAL", "SHINEDOWN"},
            {"LISTEN", "DAVID GUETTA"},
            {"PAPER GODS", "DURAN DURAN"},
            {"BLURRYFACE", "TWENTY ONE PILOTS"},
            {"Unappologetic", "Rihanna"},
            {"Native", "OneRepublic"}
        };
        
        int size = cdData.length;
        CD[] cds = new CD[size];
        for (int i = 0; i < size; i++) {
            cds[i] = new CD(cdData[i][0], cdData[i][1]);
        }
        
        ArrayListShelf arrayListShelf = new ArrayListShelf();
        TreeSetShelf treeSetShelf = new TreeSetShelf();
        HashSetShelf hashSetShelf = new HashSetShelf();

        
        long start, time;
        
        start = System.nanoTime();    
        insertTo(cds, arrayListShelf);
        time = System.nanoTime() - start;
        System.out.println("ArrayList insert: " + time);
        start = System.nanoTime();
        printCDs(size, arrayListShelf);
        time = System.nanoTime() - start;
        System.out.println("ArrayList print: " + time);

        start = System.nanoTime();    
        insertTo(cds, treeSetShelf);
        time = System.nanoTime() - start;
        System.out.println("treeSetShelf insert: " + time);
        start = System.nanoTime();
        printCDs(size, treeSetShelf);
        time = System.nanoTime() - start;
        System.out.println("treeSetShelf print: " + time);

        start = System.nanoTime();    
        insertTo(cds, hashSetShelf);
        time = System.nanoTime() - start;
        System.out.println("hasSetShelf insert: " + time);
        start = System.nanoTime();
        printCDs(size, hashSetShelf);
        time = System.nanoTime() - start;
        System.out.println("hashSetShelf print: " + time);
    }
    
    private static void insertTo(CD[] cds, Shelf shelf) {
        for (CD cd : cds) {
            shelf.insert(cd);
        }
    }
    
    private static void printCDs(int size, Shelf shelf) {
        for (int i = 0; i < size; i += 100) {
            System.out.println(shelf.get(i));
        }
    }
    
}
