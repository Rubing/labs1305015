
package lab3.cdplayer;

import java.util.ArrayList;

public class CD {
    
    private String CDName;
    private String artist;
    private ArrayList<Track> tracklist;
    
    public CD (String CDName, String artist, ArrayList<Track> tracklist) {
        this.tracklist = tracklist;  
        this.CDName=CDName;
        this.artist=artist;
    }
            
    public String getName(){
        return CDName;
    }
    
    public String getArtist(){
        return artist;
    }
    
    public Track getTrack(int index){
        return tracklist.get(index);
    }
    
    public String toString(){
        return artist + " - " + CDName ;
    }
    
    public void printTracks(){
        for (Track t : tracklist) {
            System.out.println(t);
        }
    }
    
    public int size(){
        return tracklist.size();
    }
    
}
