
package lab5.comparedatastructures;

public abstract class Shelf {
    
    public abstract void insert(CD cd);
    
    public abstract CD get(int position);
    
}
