
package lab5.comparedatastructures;

import java.util.TreeSet;

public class TreeSetShelf extends Shelf {

    private TreeSet<CD> list;
    
    public TreeSetShelf() {
        this.list = new TreeSet<CD>();
    }
    
    @Override
    public void insert(CD cd) {
        this.list.add(cd);
    }

    @Override
    public CD get(int position) {
        CD cd = this.list.first();
        int index = 0;
        while (index < position) {
            cd = this.list.higher(cd);
            index++;
        }
        return cd;
    }

}
