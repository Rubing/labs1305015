package lab3.cdplayer;

public class CDPlayer {
    
    private boolean powerOn;
    private int volume = 20;
    private boolean muted;
    private int track = -1;
    private boolean playing;
    private CD cd = null;
    
    public void powerOn(){
        powerOn=true;
        System.out.println("Power on");
    }
    
    public void powerOff(){
        powerOn=false;
        playing=false;
        System.out.println("Power off");
    }
    
    public void eject(){
        if (powerOn==false)
            return;
        cd=null;
        playing=false;
        System.out.println("Ejected CD.");
    }
    
    public void insert(CD cd){
        if (powerOn==false || this.cd != null )
            return;
        this.cd=cd;
        track = 0;
        System.out.println("Inserted " + cd);
    }
    
    public void playPause(){
        if (powerOn==false || cd == null)
            return;
        playing=!playing;
        printStatus();
    }
    
    public void stop(){
        if (powerOn==false || cd == null)
            return;
        playing=false;
        System.out.println(cd);
    }
    
    public void next(){
        if (powerOn==false || cd == null)
            return;
        if (track != cd.size() - 1){
            track ++;
        }
        printStatus();
    }
    
    public void previous(){
        if (powerOn==false || cd == null)
            return;
        if (track != 0){
            track --;
        }
        printStatus();
    }
    
    private void printStatus(){
        if (powerOn == false)
            return;
        if (playing)
            System.out.println("Now playing " + cd.getTrack(track) + " by " + cd.getArtist());
        if (muted)
            System.out.println("Muted");
    }
    
    public void volumeplus(){
        if (powerOn==false)
            return;
        volume = Math.min(100, volume + 5);
        System.out.println("Volume: " + volume);
    }
    
    public void volumeminus(){
        if (powerOn==false)
            return;
        volume = Math.max(0, volume - 5);
        System.out.println("Volume: " + volume);
    }
    
    public void mute(){
        if (powerOn==false)
            return;
        muted=true;
        System.out.println("Mute");
    }
    
    public void unmute(){
        if (powerOn==false)
            return;
        muted=false;
        System.out.println("Volume: " + volume);
    }
    
}
