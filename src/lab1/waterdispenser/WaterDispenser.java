package lab1.waterdispenser;

public class WaterDispenser {
    
    private boolean powerOn;
    private boolean lidOn;
    private int waterLevel;
    
    public WaterDispenser() {
        powerOn=false;
        lidOn=false;
        waterLevel=0;
    }
    
    public void turnOn(){
        powerOn=true;
        System.out.println("The water dispenser is on. ");
    }
    public void turnOff(){
        powerOn=false;
        System.out.println("The water dispenser is off. ");
    }
    
    public void closeLid(){
        lidOn=true;
        System.out.println("Water tank is closed.");
    }
    
    public void openLid(){
        lidOn=false;
        System.out.println("Water tank is open.");
    }
    
    public void fillWaterTank(){
        if (!lidOn) {
            waterLevel=100;
            System.out.println("Water level is now " + waterLevel);
        } else {
            System.err.println("The lid is on. Filling the water tank can not be executed.");
        }
    }
    
    public void dispenseWater(int amount){
        if (amount <= 0) {
            System.err.println("You are an idiot! Amount needs to be positive.");
        } else if (waterLevel == 0) {
            System.out.println("Tank is empty. Please fill the water tank.");
        } else if (waterLevel >= amount) {
            System.out.println("It is dispensing water.");
            waterLevel -= amount;
        } else {
            System.err.println("The water dispenser is broken.");
        }
    }
    
    
}
